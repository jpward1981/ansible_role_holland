Role Name
=========

This role sets up holland for use with borgbackup to backup a mysql database. To maximize update of a database, holland will be configured to snapshot the database's logical volumes prior to running a mysqldump.  

Requirements
------------

The MySQL database must have it's data on a volume managed by LVM.  

Role Variables
--------------

```
holland_packages:
  yum:
    - holland
    - holland-mysqldump
    - holland-mysqllvm

db_backup_user: borg
db_backup_password: password
db_host: localhost
holland_backup_dir: /var/spool/holland
holland_binlog_position: "yes"
holland_nr_backups_to_keep: 2
holland_backup_plugin: mysqldump-lvm
holland_databases: "*"
holland_exclude_databases: mysql
```

Dependencies
------------

ansible_role_packages

Example Playbook
----------------

```
- hosts: backup_clients
  roles:
     -  role: ansible_role_holland
        
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
